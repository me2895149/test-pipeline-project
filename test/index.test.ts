import { calculator } from "../index"

test('dummy test', () => {
    expect(true).toBe(true)
})

test('both are positive numbers' , () => {
    expect(calculator('*', 6, 5)).toBe(30)
})

test('with negative numbers', () => {
    expect(calculator('*', 5 , -2)).toBe(-10)
    expect(calculator('*', -4, -2)).toBe(8)
})

describe('shall we test more', () => {
    it('first test', () => {
        expect(calculator('/', 4, 2)).toBe(2)
    })
    it('second test', () => {
        expect(calculator('/', 6, 3)).toBe(2)
    })
})

test('more testing', () => {
    expect(calculator('+', 4, 2)).toBe(6)
})

test('even more testing', () => {
    expect(calculator('-', 5, 2)).toBe(3)
})